
var http = require('http');
var requestify = require('requestify');

http.globalAgent.keepAlive = true;
http.globalAgent.options.keepAlive = true;

var apiKey = `REDACTED`;

var apiBases = {
  test: `https://api-fxpractice.oanda.com`,
  prod: `https://api-fxtrade.oanda.com`
};

var currentEnvironment = 'test';
var currentAccount;

function setENV(env) {
  currentEnvironment = env;
}

function setAccount(acc) {
  currentAccount = acc;
}

function getAccounts() {
  var url = `${apiBases[currentEnvironment]}/v3/accounts`;
  return requestify.get(url, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${apiKey}`
    }
  });
}

function getAccountSummary() {
  var url = `${apiBases[currentEnvironment]}/v3/accounts/${currentAccount}/summary`;
  return requestify.get(url, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${apiKey}`
    }
  });
}

function getCandles(currency, options = {}) {
  var count = options.count || 151;
  var price = options.price || 'M';
  var granularity = options.granularity || 'M5';
  var url = `${apiBases[currentEnvironment]}/v3/instruments/${currency}/candles?count=${count}&price=${price}&granularity=${granularity}`;
  console.log(url);
  return requestify.get(url, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${apiKey}`
    }
  });
}

function createMarketOrder(currency, units, options = {}) {
  console.log('Creating market order');
  var url = `${apiBases[currentEnvironment]}/v3/accounts/${currentAccount}/orders`;
  var body = {
    order: {
      units,
      instrument:currency,
      timeInForce: "FOK",
      type: "MARKET",
      positionFill: "DEFAULT"
    }
  };
  console.log(body);
  return requestify.post(url, body, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${apiKey}`
    }
  });
}

function getAllCurrentTrades() {
  var url = `${apiBases[currentEnvironment]}/v3/accounts/${currentAccount}/trades`;
  return requestify.get(url, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${apiKey}`
    }
  });
}

function closeLongPositionsForCurrency(currency) {
  var url = `${apiBases[currentEnvironment]}/v3/accounts/${currentAccount}/positions/${currency}/close`;
  console.log("Closing position");
  console.log(url);
  return requestify.put(url, {
    longUnits: "ALL"
  }, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${apiKey}`
    }
  });
}

function closeShortPositionsForCurrency(currency) {
  var url = `${apiBases[currentEnvironment]}/v3/accounts/${currentAccount}/positions/${currency}/close`;
  console.log("Closing position");
  console.log(url);
  return requestify.put(url, {
    shortUnits: "ALL"
  }, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${apiKey}`
    }
  });
}

exports.setENV = setENV;
exports.setAccount = setAccount;
exports.getAccounts = getAccounts;
exports.getAccountSummary = getAccountSummary;
exports.getCandles = getCandles;
exports.createMarketOrder = createMarketOrder;
exports.getAllCurrentTrades = getAllCurrentTrades;
exports.closeLongPositionsForCurrency = closeLongPositionsForCurrency;
exports.closeShortPositionsForCurrency = closeShortPositionsForCurrency;