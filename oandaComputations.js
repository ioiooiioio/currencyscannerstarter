
var interface = require('./oandaInterface.js');

var cachedCandles;

var computations = {};

function computeSMA(period = 150) {
  var totalPrice = 0;
  var recentIndex = cachedCandles.length-1;
  if (!cachedCandles[recentIndex].complete) {
    recentIndex--;
  }
  var i;
  for (i=0;i<period;i++) {
    var candlePrice = parseFloat(cachedCandles[recentIndex-i].mid.c);
    totalPrice += candlePrice;
  }
  var sma = totalPrice/period;
  computations.sma = sma;
}

function computeCurrentPrice() {
  computations.currentPrice = parseFloat(cachedCandles[cachedCandles.length-1].mid.c);
}

function processCurrency(currency, cb) {
  interface.getCandles(currency).then(res => {
    cachedCandles = res.getBody().candles;
    computeSMA(144);
    computeCurrentPrice();
    cb(computations);
  }).fail(function(res) {
    console.log('API FAIL:');
    console.log(res);
    console.log(res.getCode());
    console.log(res.getBody());
  });
}

exports.processCurrency = processCurrency;