

var currencies = require('./currencyMetadata.json');
var oandaAPI = require('./oandaInterface.js');
var oandaComputations = require('./oandaComputations.js');

var currencyData = {};

oandaAPI.getAccounts().then(res => {
  var accountID = res.getBody().accounts.filter(acc => acc.mt4AccountID)[0].id;
  console.log("Setting account: "+accountID);
  oandaAPI.setAccount(accountID);
  initialize();
});

var currencyIndex = 0;

function initialize() {
  oandaAPI.getAllCurrentTrades().then(res => {
    var trades = res.getBody().trades;
    console.log(trades);
    if (trades[0]) {
      oandaAPI.closeShortPositionsForCurrency(trades[0].instrument).then(res => {
        console.log(res.getBody());
      }).fail(err => {
        console.log("Failure on closing positions");
        console.log(err);
      });
    }
  });
}

function processCurrency() {
  var currency = currencies[currencyIndex];
  //if (currency.enabled) {
    oandaComputations.processCurrency(currency.pair, res => {
      console.log(currency.pair);
      console.log(res);
      var atrDist = Math.abs((res.currentPrice-res.sma)/currency.long_atr);
      res.atrDist = atrDist;
      console.log(atrDist);
      currencyData[currency.pair] = res;
      /*if (atrDist>=1) {
        // Can place order
        var units = (res.currentPrice>res.sma)?-currency.units:currency.units;
        console.log(units);
        oandaAPI.createMarketOrder(currency.pair, units).then(res => {
          console.log("ORDER PLACED!");
          console.log(res.getBody());
        }).fail(res => {
          console.log('API FAIL:');
          console.log(res);
          console.log(res.getCode());
          console.log(res.getBody());
        });
      }*/
    });
  //}
  currencyIndex++;
  if (currencyIndex >= currencies.length) {
    currencyIndex = 0;
    setTimeout(processCurrency, 60000);
  } else {
    setTimeout(processCurrency, 2000);
  }
}

processCurrency();


